import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.StormSubmitter;
import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.testing.TestWordSpout;
import org.apache.storm.topology.IRichBolt;
import org.apache.storm.topology.IRichSpout;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.TopologyBuilder;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;
import org.apache.storm.utils.Utils;
import java.net.*;
import java.io.*;


public class ToggleCaseTopology {
    public static void main(String []args) throws Exception {
//        BufferedReader in = null;
//        //OutputStream out = null;
//        Socket sock = null;
//        String input="";
//        try {
//            sock = new Socket("localhost",5555);
//            //out = sock.getOutputStream();
//            in = new BufferedReader(new InputStreamReader(sock.getInputStream()));
//            input=(String)in.readLine();
//
//            //System.out.println(in.readLine());
//
//        }
//        catch (IOException ioe) {
//            System.err.println(ioe);
//        }

        TopologyBuilder builder=new TopologyBuilder();
        builder.setSpout("spout1",(IRichSpout) new TestSpout(),1);
        builder.setSpout("spout2",(IRichSpout) new TestSpout2(),1);
//        builder.setSpout("spout3",(IRichSpout) new TestSpout3(),1).addConfiguration("group", "b");
//        builder.setSpout("spout4",(IRichSpout) new TestSpout4(),1).addConfiguration("group", "b");
        //builder.setSpout("spout5",(IRichSpout) new TestSpout6(),1).addConfiguration("group", "b");
//        builder.setSpout("spout6",(IRichSpout) new TestSpout6(),1).addConfiguration("group", "b");
//        builder.setSpout("spout7",(IRichSpout) new TestSpout7(),1).addConfiguration("group", "b");
//        builder.setSpout("spout8",(IRichSpout) new TestSpout8(),1).addConfiguration("group", "b");
//        builder.setSpout("spout9",(IRichSpout) new TestSpout9(),1).addConfiguration("group", "b");
//        builder.setSpout("spout10",(IRichSpout) new TestSpout10(),1).addConfiguration("group", "b");

        //builder.setSpout("spout2",(IRichSpout)new MySpout(),5);
        builder.setBolt("lowercase1",(IRichBolt)new LowerCaseBolt(),1)
                .shuffleGrouping("spout1")
                .shuffleGrouping("spout2");
//        builder.setBolt("lowercase2",(IRichBolt)new LowerCaseBolt2(),1)
//                .shuffleGrouping("spout3")
//                .shuffleGrouping("spout4").addConfiguration("group", "a");
//        builder.setBolt("lowercase3",(IRichBolt)new LowerCaseBolt3(),1)
//                .shuffleGrouping("spout5")
//                .shuffleGrouping("spout6").addConfiguration("group", "b");
//        builder.setBolt("lowercase4",(IRichBolt)new LowerCaseBolt4(),1)
//                .shuffleGrouping("spout7")
//                .shuffleGrouping("spout8").addConfiguration("group", "b");
//                //.shuffleGrouping("spout5").addConfiguration("group", "a");
//        builder.setBolt("lowercase3",(IRichBolt)new LowerCaseBolt3(),1)
//                .shuffleGrouping("spout5")
//                .shuffleGrouping("spout6").addConfiguration("group", "b");
//        builder.setBolt("lowercase4",(IRichBolt)new LowerCaseBolt4(),1)
//                .shuffleGrouping("spout7")
//                .shuffleGrouping("spout8").addConfiguration("group", "b");
//        builder.setBolt("lowercase5",(IRichBolt)new LowerCaseBolt5(),1)
//                .shuffleGrouping("spout9")
//                .shuffleGrouping("spout10").addConfiguration("group", "b");

        builder.setBolt("exclamation1",(IRichBolt)new ExclamationBolt(),1)
                .shuffleGrouping("lowercase1");

//                .shuffleGrouping("lowercase3").addConfiguration("group", "b");

//        builder.setBolt("exclamation2",(IRichBolt)new ExclamationBolt2(),1)
//                .shuffleGrouping("lowercase3")
//                .shuffleGrouping("lowercase4").addConfiguration("group", "b");
//        //builder.setBolt("upperexclamation",(IRichBolt)new UpperExclamationBolt(),5)
          //      .shuffleGrouping("lowercase").shuffleGrouping("exclamation");

        Config conf=new Config();
        conf.setDebug(true);
        

            LocalCluster cluster=new LocalCluster();
            cluster.submitTopology("test",conf,builder.createTopology());
            Utils.sleep(10000);
            cluster.killTopology("test");
            cluster.shutdown();



    }

}
