import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.StormSubmitter;
import org.apache.storm.spout.SpoutOutputCollector;
import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.testing.TestWordSpout;
import org.apache.storm.topology.IRichBolt;
import org.apache.storm.topology.IRichSpout;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.TopologyBuilder;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.topology.base.BaseRichSpout;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;
import org.apache.storm.utils.Utils;

import java.util.Scanner;
import java.util.Map;

public class MySpout extends BaseRichSpout {
    private SpoutOutputCollector collector;

    @Override
    public void open(Map map,TopologyContext context,SpoutOutputCollector spoutOutputCollector) {
        collector=spoutOutputCollector;
    }

    public void nextTuple() {
        //Scanner sc=new Scanner(System.in);
        Object msgId="ID 5";
        String input="SRIDHAR";

        collector.emit(new Values(input),msgId);
    }


    @Override
    public void ack(Object id)
    {
        System.out.println("[WordSpout] ack on msgId" + id);

    }

    @Override
    public void fail(Object id)
    {
        System.out.println("[WordSpout] fail on msgId" + id);
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer)
    {
        declarer.declare( new Fields( "word1" ) );
    }


}
