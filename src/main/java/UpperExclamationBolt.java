import org.apache.storm.*;
import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.IRichBolt;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;

import java.util.Map;


public class UpperExclamationBolt implements IRichBolt {
    private OutputCollector collector;

    public void prepare(Map conf, TopologyContext context, OutputCollector collector )
    {
        this.collector = collector;
    }
    public void execute( Tuple tuple )
    {
//        if(tuple.getSourceComponent().equals("spout1")) {
//            System.out.println("Spout");
//        }
//        else if(tuple.getSourceComponent().equals("lowercase")) {
//            System.out.println("Lowercase");
//        }
//        else if(tuple.getSourceComponent().equals("exclamation")) {
//            System.out.println("Exclamation");
//        }
        String temp = tuple.getString(0);
        temp=temp.toUpperCase();
        //System.out.println(temp);
        collector.ack( tuple );
    }

    @Override
    public void cleanup() {

    }

    public void declareOutputFields( OutputFieldsDeclarer declarer )
    {
        declarer.declare( new Fields( "word" ) );
    }

    @Override
    public Map<String, Object> getComponentConfiguration() {
        return null;
    }

}
