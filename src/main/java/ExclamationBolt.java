import org.apache.storm.*;
import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.IRichBolt;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;

import java.util.Map;


public class ExclamationBolt implements IRichBolt {
    private OutputCollector collector;
    String temp;
    public void prepare(Map conf, TopologyContext context, OutputCollector collector )
    {
        this.collector = collector;
    }
    public void execute( Tuple tuple )
    {
        temp = tuple.getStringByField("word");
        temp=temp+"!!!";
        collector.ack(tuple);
//        System.out.println(temp);
//        collector.emit(new Values(temp));
//        collector.ack( tuple );
    }

    @Override
    public void cleanup() {
   //         System.out.println("-----Final Result ExB1-----");
   //         System.out.println(temp);
   //         System.out.println("----------");

    }
    @Override
    public void declareOutputFields( OutputFieldsDeclarer declarer )
    {
       //declarer.declare( new Fields( "word" ) );
    }

    @Override
    public Map<String, Object> getComponentConfiguration() {
        return null;
    }



}
