import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.StormSubmitter;
import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.testing.TestWordSpout;
import org.apache.storm.topology.IRichBolt;
import org.apache.storm.topology.IRichSpout;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.TopologyBuilder;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;
import org.apache.storm.utils.Utils;

import java.util.Map;


public class LowerCaseBolt2 implements IRichBolt {

    private OutputCollector collector;

    public void prepare(Map conf, TopologyContext context, OutputCollector collector )
    {
        this.collector = collector;
    }
    public void execute( Tuple tuple )
    {
        String lowercase="";
        if(tuple.getSourceComponent().equals("spout1")) {
            lowercase+=tuple.getStringByField("word");
            lowercase=lowercase.toLowerCase();

        }
        else {
            lowercase+=tuple.getStringByField("word");
            lowercase=lowercase.toLowerCase();
        }

        //System.out.println(lowercase);
        collector.emit(new Values(lowercase));
        collector.ack(tuple);
    }

    @Override
    public void cleanup() {

    }
    @Override
    public void declareOutputFields( OutputFieldsDeclarer declarer )
    {
        declarer.declare( new Fields( "word" ) );
    }

    @Override
    public Map<String, Object> getComponentConfiguration() {
        return null;
    }


}
