import org.apache.logging.log4j.core.config.plugins.util.ResolverUtil;
import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.StormSubmitter;
import org.apache.storm.spout.SpoutOutputCollector;
import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.testing.TestWordSpout;
import org.apache.storm.topology.IRichBolt;
import org.apache.storm.topology.IRichSpout;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.TopologyBuilder;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.topology.base.BaseRichSpout;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;
import org.apache.storm.utils.Utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.Scanner;
import java.util.Map;



public class TestSpout extends BaseRichSpout  {

    //String input;
    BufferedReader in = null;
    //OutputStream out = null;
    Socket sock = null;
    String input="";

    //    public TestSpout(String val) {
//        input=val;
//    }
    public TestSpout() throws Exception {

            input="venkat";

    }

    private SpoutOutputCollector collector;
    String componentId;
    @Override
    public void open(Map map,TopologyContext context,SpoutOutputCollector spoutOutputCollector) {
        //System.out.println("In open() of Spout1");
        collector=spoutOutputCollector;
        componentId=context.getThisComponentId();

//        try {
//            sock = new Socket("localhost",5555);
//            //out = sock.getOutputStream();
//            in = new BufferedReader(new InputStreamReader(sock.getInputStream()));
//            input=(String)in.readLine();
//            //System.out.println("In open() of Spout1");
//
//
//        }
//        catch (IOException ioe) {
//            System.err.println(ioe);
//        }
//        finally {
//            try {
//                in.close();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            try {
//                sock.close();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//
//        }
    }

    public void nextTuple() {
        //Scanner sc=new Scanner(System.in);
        //input=componentId + input;
        //System.out.println(componentId + input);
        collector.emit(new Values(input));
    }


    @Override
    public void ack(Object id)
    {
    }

    @Override
    public void fail(Object id)
    {
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer)
    {
        declarer.declare( new Fields( "word" ) );
    }






}
