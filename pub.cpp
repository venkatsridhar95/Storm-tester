#include <stdio.h>
#include <pthread.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/time.h>
#include <iostream>
#include <map>
#include <vector>
#define NTHREADS 8
#define TRUE   1
#define FALSE  0
#define PORT 5555
using namespace std;

pthread_t thread_id[NTHREADS];
int master_socket;
int NextThread=0;
struct sockaddr_in address;
int opt=TRUE;
class CalClientConnection
{
	pthread_t *publisherThread;
	int socket;
	public:
	CalClientConnection(pthread_t *t,int s)
	{
		publisherThread=t;
		socket=s;

	}
	bool HasDataToRead()
	{
		return true;
	}
	bool onReadyToRead()
	{
		return true;
	}
	bool DoIsSet(fd_set *rfs)
	{
		return FD_ISSET(socket,rfs);
	}
	int DoAddSocket(int smax,fd_set *rfs)
	{
		FD_SET(socket,rfs);
		if(smax < socket)
			smax=socket;
		return smax;
	}

};
map<pthread_t,CalClientConnection *> CalClientList;
map<pthread_t,vector<CalClientConnection *> > newCalClientList;
int do_add_fdset(map<pthread_t,CalClientConnection *> &list,int smax,fd_set *rfs)
{
	map<pthread_t,CalClientConnection *> :: iterator curr = list.begin();
	map<pthread_t,CalClientConnection *> :: iterator end = list.end();
	for(;curr!=end; ++curr)
	{
		CalClientConnection *connection=curr->second;

		if(connection->HasDataToRead())
		{
			smax=connection->DoAddSocket(smax,rfs);
		}


	}
	return smax;

}
void printmap(map<pthread_t,CalClientConnection *> &list)
{
	map<pthread_t,CalClientConnection *> :: iterator curr = list.begin();
	map<pthread_t,CalClientConnection *> :: iterator end = list.end();
	for(;curr!=end;++curr)
	{
		cout<<curr->first<<"\t"<<curr->second<<"\n";
	}

}
void printmapnew(map<pthread_t,vector<CalClientConnection *> > &list)
{
	map<pthread_t,vector<CalClientConnection *> > :: iterator curr = list.begin();
	map<pthread_t,vector<CalClientConnection *> > :: iterator end = list.end();
	for(;curr!=end;++curr)
	{
		vector<CalClientConnection *> temp=curr->second;
		cout<<"Thread:"<<curr->first<<"\t";
		cout<<"connections:";
		for(int i=0;i<temp.size();i++)
			cout<<temp[i]<<"\t";
		cout<<"\n";
	}

}
void do_proc_fdset(map<pthread_t,CalClientConnection *> &list,fd_set *rfs)
{
	map<pthread_t,CalClientConnection *> :: iterator curr = list.begin();
	map<pthread_t,CalClientConnection *> :: iterator end = list.end();
	for(;curr!=end;++curr)
	{
		CalClientConnection *connection=curr->second;
		if(connection->DoIsSet(rfs))
		{
			if(!connection->onReadyToRead())
			{
				list.erase(curr);
				delete connection;
			}
		}

	}
}
class CPublisherThread
{
	pthread_t pubthread;
	int id;
public:
	CPublisherThread(pthread_t t,int val) {
		pubthread=t;
		id=val;
	}
	void *run()
	{
		void *result=0;
		try
		{
			cout<<"Running thread: "<<this->id<<"\n";

			run_parent_loop();

			cout<<"Thread"<<id<<"Terminated Normally"<<"\n";
		}
		catch(...)
		{
			cout<<"Exception in thread"<<"\n";
			exit(0);
		}
		return result;
	}
	CalClientConnection *AcceptConnection(pthread_t currThread)
	{
		CalClientConnection *connection=NULL;
		int addrlen=sizeof(address);
		int new_socket;
		if ((new_socket = accept(master_socket, (struct sockaddr *)&address, (socklen_t*)&addrlen))<0) 
		{
       		perror("accept");
        	return connection;
    	}
    	struct linger li;
    	li.l_onoff=1;
    	li.l_linger=0;
    	if ( setsockopt(new_socket, SOL_SOCKET, SO_LINGER, &li, sizeof(li)) < 0 ) 
    	{
        	perror("setsockopt");
        	close(new_socket);
        	return connection;
    	}

    	cout<<"New connection accepted in threead"<<"\t"<<currThread<<"\n";
    	connection=new CalClientConnection(&currThread,new_socket);
    	return connection;

	}
	void run_parent_loop()
	{
		while(1)
		{
			fd_set rfs;
			FD_ZERO(&rfs);
			FD_SET(master_socket,&rfs);
			int smax=master_socket;
			smax=do_add_fdset(CalClientList,smax,&rfs);	
			int sel=select(smax+1,&rfs,NULL,NULL,NULL);
			if ((sel < 0) && (errno!=EINTR)) 
			{
            	printf("select error");
    		}
    		else if(sel > 0)
    		{
    			if(FD_ISSET(master_socket,&rfs))
    			{
    				CalClientConnection *pData_log=AcceptConnection(this->pubthread);
    				if(pData_log!=NULL)
	    				CalClientList.insert(pair <pthread_t,CalClientConnection *>(this->pubthread,pData_log));

    			}
    			do_proc_fdset(CalClientList,&rfs);
    		}


		}
		

	}

};

static int InitializeServerSocket() 
{
	master_socket=socket(AF_INET, SOCK_STREAM , 0);
	address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons( PORT );
    
    if (bind(master_socket, (struct sockaddr *)&address, sizeof(address))<0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }
    printf("Listener on port %d \n", PORT);
    if (listen(master_socket, 300) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    return master_socket; 
}
void *InitThread(void *params)
{
	static int count=1;
	//int id=*(int *)params;
	//printf("Thread number %ld\n", pthread_self());
	pthread_t currThread=pthread_self();
	CPublisherThread *p=new CPublisherThread(currThread,count);
	count++;
	//p->run();
	
	return 0;
}
void InitAndCreateThreads() 
{
	for(int i=0;i<NTHREADS;i++)
		pthread_create(&thread_id[i],NULL,InitThread,NULL);
	
}
CalClientConnection *AcceptConnection(pthread_t *publisherThread,int threadIndex)
{
	CalClientConnection *connection =NULL;
	int addrlen=sizeof(address);
	int new_socket;
	if ((new_socket = accept(master_socket, (struct sockaddr *)&address, (socklen_t*)&addrlen))<0) 
	{
        perror("accept");
        return connection;
    }
    struct linger li;
    li.l_onoff=1;
    li.l_linger=0;
    if ( setsockopt(new_socket, SOL_SOCKET, SO_LINGER, &li, sizeof(li)) < 0 ) {
        perror("setsockopt");
        close(new_socket);
        return connection;
    }
    //pthread_t temp=pthread_self();
    cout<<"New connection in thread"<<threadIndex<<"\t"<<*publisherThread<<"\n";
    connection=new CalClientConnection(publisherThread,new_socket);

	return connection;
}
void WaitForAllThreadsToTerminate() 
{
	while(1)
	{
		//int ct=1;
		vector<CalClientConnection *> conns;
		if(NextThread>=NTHREADS)
		NextThread=0;
		fd_set rfs;
		FD_ZERO(&rfs);
		FD_SET(master_socket,&rfs);
		int smax=master_socket;
		int sel=select(smax+1,&rfs,NULL,NULL,NULL);
		if ((sel < 0) && (errno!=EINTR)) {
	            printf("select error");
	    }
	    else if(sel > 0)
	    {
	    	if(FD_ISSET(master_socket,&rfs))
		    {
		    	CalClientConnection *pData_log = AcceptConnection(&thread_id[NextThread],NextThread);
		    	if(pData_log!=NULL)
		    	{
		    		conns=newCalClientList[thread_id[NextThread]];
		    		conns.push_back(pData_log);
		    		newCalClientList[thread_id[NextThread]]=conns;
		    		CalClientList.insert(pair <pthread_t,CalClientConnection *>(thread_id[NextThread],pData_log));
		    		//cout<<pData_log<<"\n";
		    		//printmap(CalClientList);
		    		printmapnew(newCalClientList);
		    		NextThread++;	
		    	}
		    	
	    	}
	    }	
	}
	
}

int main() 
{
	master_socket=InitializeServerSocket();
	InitAndCreateThreads();
	WaitForAllThreadsToTerminate();

	

	return 0;
}
