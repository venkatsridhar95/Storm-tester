#include <stdio.h>
#include <pthread.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/time.h>

#define NTHREADS 8
#define TRUE   1
#define FALSE  0
#define PORT 5555

void *connection_handler(void *socket_desc) {

    int sock=*(int *)socket_desc;
    int read_size;
    char *message, client_message[2000];
    printf("Thread number %ld\n", pthread_self());
    message="VENKAT";
    if(send(sock,message,strlen(message),0)==-1)
         perror("send failed");
    printf("Sent\n");
    close(sock);
    return 0;

}
int main(int argc , char *argv[])
{
    int opt = TRUE;
    int master_socket , addrlen , new_socket , client_socket[10] , max_clients = 10 ;
    int activity, i , valread , sd;
    int max_sd;
    struct sockaddr_in address;
    //pthread_t thread_id;
    pthread_t thread_id[NTHREADS];
    int nextThreadId=0;
    char buffer[1025];
    fd_set readfds;
    char *message = "Test \r\n";
    for (i = 0; i < max_clients; i++) {
        client_socket[i] = 0;
    }
    if ( (master_socket = socket(AF_INET , SOCK_STREAM , 0)) == 0) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }
    if ( setsockopt(master_socket, SOL_SOCKET, SO_REUSEADDR, (char *)&opt, sizeof(opt)) < 0 ) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons( PORT );
    if (bind(master_socket, (struct sockaddr *)&address, sizeof(address))<0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }
    printf("Listener on port %d \n", PORT);
    if (listen(master_socket, 3) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }
    addrlen = sizeof(address);
    puts("Waiting for connections ...");
    while(TRUE) {
        FD_ZERO(&readfds);
        FD_SET(master_socket, &readfds);
        max_sd = master_socket;
        for ( i = 0 ; i < max_clients ; i++) {
            sd = client_socket[i];
            if(sd > 0)
                FD_SET( sd , &readfds);
            if(sd > max_sd)
                max_sd = sd;
        }
        activity = select( max_sd + 1 , &readfds , NULL , NULL , NULL);
        if ((activity < 0) && (errno!=EINTR)) {
            printf("select error");
        }
        if (FD_ISSET(master_socket, &readfds)) {
            int count=0;
            if ((new_socket = accept(master_socket, (struct sockaddr *)&address, (socklen_t*)&addrlen))<0) {
                perror("accept");
                exit(EXIT_FAILURE);
            }
            // if( send(new_socket, message, strlen(message), 0) != strlen(message) )
            // {
            //     perror("send");
            // }
            //printf("New connection , socket fd is %d , ip is : %s , port : %d \n" , new_socket , inet_ntoa(address.sin_addr) , ntohs(address.sin_port));
            //puts("Welcome message sent successfully");

            // for (i = 0; i < max_clients; i++)
            // {

            //     if( client_socket[i] == 0 )
            //     {
            //         client_socket[i] = new_socket;
            //         printf("Adding to list of sockets as %d\n" , i);

            //         break;
            //     }
            // }
            if(nextThreadId>=NTHREADS) {
               nextThreadId=0;
               for(int j=0; j < NTHREADS; j++)
                 pthread_join( thread_id[j], NULL);

            }
            printf("nextThreadId:%d",nextThreadId);
            if(pthread_create(&thread_id[nextThreadId],NULL,connection_handler,(void *)&new_socket) < 0) {
                perror("could not create thread");
                exit(EXIT_FAILURE);
            }
            nextThreadId++;
            // printf("Handler: %d assigned",count++);
        }
    }

    return 0;
}
