#include <thread>
#include<iostream>
#include <stdio.h>
#include <pthread.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/time.h>
#define TRUE   1
#define FALSE  0
#define PORT 5555
#define NTHREADS 8
int client_socket[10];
int max_clients=10;
fd_set readfds;
int i;
//using namespace std;

void func(int sock)
{
    std::cout<<"socket"<<sock<<"\t";
    std::thread::id this_id=std::this_thread::get_id();
    std::cout<<this_id<<"\n";
    int read_size;
    char buffer[1025];
    char *message, client_message[2000];
    int valread;

    while(1) {
        if (FD_ISSET( sock , &readfds)) {
            if((valread=read(sock,buffer,1024))==0) {
                printf("\nHost Disconnected");
                close(sock);
                client_socket[i]=0;
            }
            else {
              //int sock=*(int *)socket_desc;
                buffer[valread]='\0';
                send(sock,buffer,strlen(buffer),0);
            }
        }
    }
}

int main()
{
    std::thread t[NTHREADS];
  //  for(int i=0;i<NTHREADS;i++) {
  //     t[i]=std::thread(func,i);
  //     //std::cout<<"\n";
  //  }
    int opt = TRUE;
    int addrlen , new_socket , activity, i , valread , sd;
    int max_sd;
    int master_socket;
    struct sockaddr_in address;
    int nextThreadId=0;
    char buffer[1025];
    char *message = "Test \r\n";
    for (i = 0; i < max_clients; i++) {
        client_socket[i] = 0;
    }
    if( (master_socket = socket(AF_INET , SOCK_STREAM , 0)) == 0) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }
    if( setsockopt(master_socket, SOL_SOCKET, SO_REUSEADDR, (char *)&opt, sizeof(opt)) < 0 ) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons( PORT );

    if (bind(master_socket,(struct sockaddr *)&address, sizeof(address))<0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }
    printf("Listener on port %d \n", PORT);

    if (listen(master_socket, 3) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    addrlen = sizeof(address);
    puts("Waiting for connections ...");

    //InitAndCreateThreads();
    //WaitForAllThreadsToTerminate();

    while(TRUE) {

        FD_ZERO(&readfds);
        FD_SET(master_socket, &readfds);
        max_sd = master_socket;
        for ( i = 0 ; i < max_clients ; i++) {
            sd = client_socket[i];
            if(sd > 0)
                FD_SET( sd , &readfds);
            if(sd > max_sd)
                max_sd = sd;
        }
        activity = select( max_sd + 1 , &readfds , NULL , NULL , NULL);
        if ((activity < 0) && (errno!=EINTR)) {
            printf("select error");
        }
        if (FD_ISSET(master_socket, &readfds)) {
            //int count=0;
            if ((new_socket = accept(master_socket, (struct sockaddr *)&address, (socklen_t*)&addrlen))<0) {
                perror("accept");
                exit(EXIT_FAILURE);
            }
            if( send(new_socket, "hi", 2, 0) != 2 ) {
                perror("send");
            }
            puts("Welcome message sent successfully");
            for (i = 0; i < max_clients; i++) {
                if( client_socket[i] == 0 ) {
                    client_socket[i] = new_socket;
                    printf("Adding to list of sockets as %d\n" , i);
                    break;
                }
            }
            // if(nextThreadId>=NTHREADS)
            // {
            //    nextThreadId=0;
            //     // for(int i=0;i<NTHREADS;i++) {
            //     //    t[i].join();
            //     // }
            //
            //    //for(int j=0; j < NTHREADS; j++)
            //     //  pthread_join( thread_id[j], NULL);
            //
            // }

            printf("nextThreadId:%d",nextThreadId%NTHREADS);
            std::cout<<"\n"<<&t[nextThreadId%NTHREADS]<<"\n";
            t[nextThreadId%NTHREADS]=std::thread(func,new_socket);
            t[nextThreadId%NTHREADS].detach();
             //t[nextThreadId%NTHREADS]=std::thread(func);
            nextThreadId++;
        }
    }
   return 0;
}
